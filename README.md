Run Project:

python -m venv [name-virtual_enviroment] |
pip install django |
pip install djangorestframework |
pip install django-rest-swagger |
pip install pyyaml uritemplate |

RUN python mange.py runserver

URL's:

127.0.0.1:8000/admin (Admin Django to manage Database) |
127.0.0.1:8000/swagger-ui (Swagger User Interface for API) |
127.0.0.1:8000 (API Root)

Consume WS via **requests** python Package : pip install requests  

**Example :** 

    rq = requests.get('url ws')
    rq.json()

    


