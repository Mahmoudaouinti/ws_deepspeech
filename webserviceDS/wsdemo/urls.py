"""webserviceDS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""


from django.conf.urls import url, include
from rest_framework import routers
from wsdemo import views
from django.views.generic import TemplateView
from rest_framework.schemas import get_schema_view



# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'languages', views.LanguageViewSet)
router.register(r'models', views.ModelsViewSet)
router.register(r'versions', views.VersionViewSet)
router.register(r'reviews', views.ReviewViewSet)


from django.urls import path, include

urlpatterns = [
    path('', include(router.urls)),

    path('swagger-ui/', TemplateView.as_view(
        template_name='swagger-ui.html',
        extra_context={'schema_url':'openapi-schema'}
    ), name='swagger-ui'),


    path('openapi', get_schema_view(
        title="Deepspeech API",
        description="API for all things …",
        version="1.0.0"
    ), name='openapi-schema'),
    #path('hello/',views.hello_world, name='hello'),
    url('^modelsDS/(?P<language_code>[-\w]+)/$',views.Detail_model.as_view(), name='modelsDS')

]


