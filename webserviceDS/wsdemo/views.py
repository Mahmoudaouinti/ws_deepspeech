from django.conf.urls import url, include
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets, generics, authentication, permissions
from rest_framework.views import APIView
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from wsdemo.models import Language, Modelds, Version, Review
from drf_queryfields import QueryFieldsMixin
#from .utils import transcribe


# Serializers define the API representation.
class LanguageSerializer(QueryFieldsMixin , serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = ['label']

# ViewSets define the view behavior.
class LanguageViewSet(viewsets.ModelViewSet):
    queryset = Language.objects.all()
    serializer_class = LanguageSerializer



class ModelsSerializer(QueryFieldsMixin , serializers.ModelSerializer):
    language = LanguageSerializer(required=False)
    class Meta:
        model = Modelds
        fields = ['description', 'statut', 'language']


class ModelsViewSet(viewsets.ModelViewSet):
    queryset = Modelds.objects.all()
    serializer_class = ModelsSerializer



class VersionSerializer(QueryFieldsMixin , serializers.ModelSerializer):
    modelds = ModelsSerializer(required=False)
    class Meta:
        model = Version
        fields = ['modelds', 'num_version', 'file_url', 'description', 'average_note']


class VersionViewSet(viewsets.ModelViewSet):
    queryset = Version.objects.all()
    serializer_class = VersionSerializer




class ReviewSerializer(QueryFieldsMixin , serializers.ModelSerializer):
    version = VersionSerializer(required=False)
    class Meta:
        model = Review
        fields = ['version', 'note']



class ReviewViewSet(viewsets.ModelViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer

class Detail_model(generics.ListAPIView):
    queryset = Version.objects.none()
    serializer_class = VersionSerializer

    def get_queryset(self, *args, **kwargs):
        #import pdb;pdb.set_trace()
        return Version.objects.filter(modelds__language__label=self.kwargs.get('language_code'))

# Test @api_view django
'''
@api_view(['GET', 'POST'])
@permission_classes((permissions.AllowAny,))
def hello_world(request):
    if request.method == 'POST':
        #import pdb;pdb.set_trace()
        #transcribe(filename)

        return Response({"message": "Got some data!", "data": request.data})
    return Response({"message": "c"})
'''

