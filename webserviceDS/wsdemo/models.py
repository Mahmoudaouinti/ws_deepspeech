from django.db import models

# Create your models here.
from django.db.models import Avg
from django.db import models
from django.utils.translation import ugettext_lazy as _


from django.utils import timezone
# Create your models here.


class Language(models.Model):

    label = models.CharField(max_length=20)
    description = models.CharField(max_length=255)

    def __str__(self):
        return "{}".format(self.label)

class Modelds(models.Model):
    STATUSES = (
        ("draft", _("brouillon")),
        ("processed", _("traité")),
        ("archived", _("archivé"))
    )

    description = models.CharField(max_length=255)
    statut = models.CharField(
        choices=STATUSES,
        default="draft",
        max_length=50
    )
    language = models.ForeignKey(
        Language,
        on_delete=models.CASCADE)

    def __str__(self):
        return "{} {}".format(self.language, self.description)


class Version(models.Model):

    modelds = models.ForeignKey(
        Modelds,
        on_delete=models.CASCADE)
    num_version = models.IntegerField(default=0.0)
    build = models.IntegerField(default=0.0)
    file_url = models.FileField(
        upload_to="mod_folder/"
    )
    date = models.DateField()
    description = models.CharField(max_length=255)

    def __str__(self):
        return "{} {}".format(self.modelds, self.num_version)

    @property
    def average_note(self):
        if self.review_set.count():
            return list(self.review_set.aggregate(avg=Avg('note')).values())[0]

class Review(models.Model):

    version = models.ForeignKey(
        Version,
        on_delete=models.CASCADE)
    date = models.DateField()
    note = models.IntegerField(default=0.0)
    rmq = models.CharField(max_length=255)

    def __str__(self):
        return "{}".format(self.note)



