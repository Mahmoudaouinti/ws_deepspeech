import os
import ffmpeg
import uuid
import time
import sys
from deepspeech import Model
import scipy.io.wavfile as wav
from werkzeug.utils import secure_filename

def transcribe(filename):
    global transcription_in_progress
    if(transcription_in_progress):
        print("Oh no! Another transcription was in progress, waiting 5 seconds...")
        time.sleep(5)
        transcribe(filename)
    print("Starting transcription...")
    transcription_in_progress = True
    fs, audio = wav.read(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    processed_data = ds.stt(audio)
    os.remove(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    transcription_in_progress = False
    return processed_data

def normalize_file(file):
    filename = str(uuid.uuid4()) + ".wav"
    fileLocation = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    stream = ffmpeg.input(file)
    stream = ffmpeg.output(stream, fileLocation, acodec='pcm_s16le', ac=1, ar='16k')
    ffmpeg.run(stream)
    return filename

def api_transcribe():
    # check and see if API keys are set
    if(len(api_keys) > 0):
        # get the request headers and check the keys
        if "Authorization" in request.headers:
            key = request.headers["Authorization"]
            if(len(key) > 7):
                if key[7:] not in api_keys:
                    return make_response(jsonify({'error': 'Your API key is invalid :c'}), 400)
            else:
                return make_response(jsonify({'error': 'Your API key was empty :c'}), 400)
        else:
            return make_response(jsonify({'error': 'We need an API key to process your request :c'}), 400)

    # check if the post request has the file part
    if 'file' not in request.files:
        return make_response(jsonify({'error': 'No file part'}), 400)
    file = request.files['file']
    # if the request has a blank filename
    if file.filename == '':
        return make_response(jsonify({'error': 'No file in file parameter'}), 400)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        fileLocation = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(fileLocation)
        convertedFile = normalize_file(fileLocation)
        # stream = ffmpeg.overwrite_output(stream)
        os.remove(fileLocation)

        return jsonify({'message' : transcribe(filename=convertedFile)})
    return make_response(jsonify({'error': 'Something went wrong :c'}), 400)