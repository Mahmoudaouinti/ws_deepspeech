from django.contrib import admin

# Register your models here.

from .models import Language, Modelds,Version, Review

admin.site.register(Language)
admin.site.register(Modelds)
admin.site.register(Version)
admin.site.register(Review)